//
//  Token.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "Token.h"

@implementation Token

@synthesize Type;
@synthesize Value;

-(id) initWithTokenType:(NSString *)type withValue:(NSString *)value
{
    if(self = [super init])
    {
        Type = type;
        Value = value;
    }
    
    return self;
}
@end
