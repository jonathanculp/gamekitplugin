//
//  Tokenizer.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Token.h"

NS_ASSUME_NONNULL_BEGIN

@interface Tokenizer : NSObject
{
    unichar* buffer;
    unichar c;
    int len;
    int itr;
    int mode;
    BOOL doneParsing;
    NSCharacterSet* identifierChars;
    NSCharacterSet* typeChars;
}

-(id) initWithString:(NSString*) decl;
-(BOOL) isDoneParsing;
-(void) next_c;
-(BOOL) isId;
-(BOOL) isType;
-(BOOL) isWhiteSpace;
-(Token*) ParseType;
@end

NS_ASSUME_NONNULL_END
