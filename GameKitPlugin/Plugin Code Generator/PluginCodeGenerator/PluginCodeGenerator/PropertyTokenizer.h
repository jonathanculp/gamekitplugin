//
//  PropertyParser.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tokenizer.h"
#import "Token.h"

NS_ASSUME_NONNULL_BEGIN

@interface PropertyTokenizer : Tokenizer
-(Token *) Next;
-(void) ConsumeJunk;
-(Token *) PropertyAttribute;
-(Token *) PropertyType;
-(Token *) PropertyName;
@end

NS_ASSUME_NONNULL_END
