//
//  JSONLoader.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/15/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "JSONLoader.h"

@implementation JSONLoader

+(id) LoadJSONData:(NSString*) filename
{
    NSData* json = [NSData dataWithContentsOfFile:filename];
    
    if(json == nil)
    {
        NSLog(@"Unable to load file %@", filename);
    }
    
    NSError *error = nil;
    id classData = [NSJSONSerialization
                          JSONObjectWithData:json
                          options:0
                          error:&error];
    
    if(classData == NULL)
    {
        NSException *exception =
           [NSException exceptionWithName:@"JsonDataException"
                        reason:[error localizedFailureReason] userInfo:nil];

        @throw exception;
    }
    
    return classData;
}

@end
