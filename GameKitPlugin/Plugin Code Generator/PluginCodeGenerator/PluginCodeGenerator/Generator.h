//
//  Generator.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 12/31/19.
//  Copyright © 2019 HovelHouseApps. All rights reserved.
//

#ifndef Generator_h
#define Generator_h

#import "GRMustache.h"

void generateClass(NSString* className);
void generateEnum(NSString* enumName);
void generateCallbacks(NSString* callbackFilename);
GRMustacheTemplate* LoadTemplate(NSString* templateName);
void DumpObjcMethods(Class clz);
NSDictionary* MakeData(Class clz, NSArray* methods);

void renderTemplateToFile(NSDictionary* data, GRMustacheTemplate* template, NSString* filename, NSString* outputDir);

#endif /* Generator_h */
