//
//  Utils.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 2/17/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "Utils.h"
#import "JSONLoader.h"

@implementation Utils
{

}

+(BOOL) IsArray:(NSString*) Type
{
    if([Type length] < 9)
        return NO;
    
    NSString* substr = [Type substringToIndex:8];
                        
    if([substr isEqualToString:@"NSArray<"] == NO)
        return NO;
    
    return YES;
}

+(BOOL) IsUntypedArray:(NSString*) type
{
    return [type isEqualToString:@"NSArray*"];
}

+(BOOL) IsArrayOfPointers:(NSString*) Type
{
    BOOL isArray = [self IsArray:Type];
    if(isArray == NO)
        return NO;
    
    NSString* lastChar = [Type substringWithRange:NSMakeRange(Type.length - 3, 1)];
    
    return [lastChar isEqualToString:@"*"];
}

+(BOOL) IsDictionary:(NSString *)Type
{
    return [Type hasPrefix:@"NSDictionary"];
}

+(BOOL) IsPointer:(NSString *)Type
{
    return [Type hasSuffix:@"*"];
}

+(BOOL) IsSet:(NSString *)Type
{
    return [Type hasPrefix:@"NSSet"];
}

+(NSString*) DictionaryKeyType:(NSString*) Type
{
    NSString* regEx = [NSString stringWithFormat:@"<.*,"];
    NSRange range = [Type rangeOfString:regEx options:NSRegularExpressionSearch];
    if(range.location == NSNotFound)
        return @"object";
    range.location += 1;
    range.length -= 2;
    return [Type substringWithRange:range];
}

+(NSString*) DictionaryValueType:(NSString*) Type
{
    NSString* regEx = [NSString stringWithFormat:@",.*>"];
    NSRange range = [Type rangeOfString:regEx options:NSRegularExpressionSearch];
    if(range.location == NSNotFound)
        return @"";
    range.location += 1;
    range.length -= 2;
    return [Type substringWithRange:range];
}

+(BOOL) IsStringArray:(NSString *)Type
{
    static NSSet* StringArrayTypes = nil;
    
    if(StringArrayTypes == nil)
        StringArrayTypes = [NSSet setWithObjects:
        @"NSArray<CKRecordFieldKey>*",
        @"NSArray<NSString*>*",
        @"NSArray<CKOperationID>*",
        @"NSArray<CKSubscriptionID>*",
        nil];
    
    return [StringArrayTypes containsObject:Type];
}

+(BOOL) IsBlock:(NSString*) Type
{
    static NSMutableSet* BlockTypes = nil;
    
    if(BlockTypes == nil)
    {
        NSDictionary* BlockData = [JSONLoader LoadJSONData:@"./json/Data/BlockData.json"];
        
        BlockTypes = [[NSMutableSet alloc] init];
        for (NSDictionary* block in BlockData[@"Blocks"]) {
            [BlockTypes addObject:block[@"Type"]];
        }
    }
    
    return [BlockTypes containsObject:Type];
}

+(BOOL) IsMethodBlock:(NSString*) Type
{
    static NSMutableSet* MethodBlockType = nil;
    
    if(MethodBlockType == nil)
    {
        NSDictionary* BlockData = [JSONLoader LoadJSONData:@"./json/Data/BlockData.json"];
        
        MethodBlockType = [[NSMutableSet alloc] init];
        for (NSDictionary* block in BlockData[@"Blocks"]) {
            if(block[@"Method"] != nil)
                [MethodBlockType addObject:block[@"Type"]];
        }
    }
    
    return [MethodBlockType containsObject:Type];
}

+(BOOL) IsInstanceType:(NSString *)Type
{
    return [Type isEqualToString:@"instancetype"];
}


// trims off the "NSArray<>*" bit of a string, uses character counts, so not very reliable
// especially if there's some whitespace differences
+(NSString*) ArraySubType:(NSString*) arrayDef
{
    // 8 = num chars in 'NSArray<', the extra 2 is for the '>*' at the end
    long span = [arrayDef length] - 8 - 2;
    NSString* subtype = [arrayDef substringWithRange:NSMakeRange(8, span)];
    return subtype;
}

// trims off the "NSArray<>*" bit of a string, uses character counts, so not very reliable
// especially if there's some whitespace differences
+(NSString*) SetSubType:(NSString*) setDef
{
    // 6 = num chars in 'NSSet<', the extra 2 is for the '>*' at the end
    long span = [setDef length] - 6 - 2;
    NSString* subtype = [setDef substringWithRange:NSMakeRange(6, span)];
    return subtype;
}

// trims off the "NSDictionary<>*" bit of a string, uses character counts, so not very reliable
// especially if there's some whitespace differences
+(NSString*) DictValueType:(NSString*) dictDef
{
    int idx0 = -1;
    int idx1 = -1;
    
    for(int i = 0; i < [dictDef length]; i++)
    {
        if([dictDef characterAtIndex:i] == ',')
        {
            idx0 = i + 1;
            break;
        }
    }
    
    for(int i = [dictDef length] -1; i >= 0; i--)
    {
        if([dictDef characterAtIndex:i] == '>')
        {
            idx1 = i;
            break;
        }
    }
    
    if(idx1 > idx0)
        return [dictDef substringWithRange:NSMakeRange(idx0, idx1 - idx0)];
    else
        return @"DictValueType_Error";
}

@end
