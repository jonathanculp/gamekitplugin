//
//  JSONLoader.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/15/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface JSONLoader : NSObject
+(id) LoadJSONData:(NSString*) filename;
@end

NS_ASSUME_NONNULL_END
