//
//  Converters.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/20/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"

NS_ASSUME_NONNULL_BEGIN

@interface Converters : NSObject
{
    NSDictionary* BlockData;
    GRMustacheTemplate* BlockTemplate;
    GRMustacheTemplate* CSharpBlockTemplate;
    GRMustacheTemplate* CSharpPropertyBlockTemplate;
    GRMustacheTemplate* ObjCPropertyBlockTemplate;
    GRMustacheTemplate* ArgumentTemplate;
    NSSet* StringTypes;
    NSSet* PointerTypes;
    NSSet* DateTypes;
    NSSet* StringArrayTypes;
    NSSet* EnumTypes;
    int NumTimesSeenArrayParam;
    int NumArgs;
}

-(NSMutableDictionary*) GetBlockData:(NSString*) blockName;
-(GRMustacheTemplate*) LoadTemplate:(NSString*) filename;
-(NSString*) ObjCToCType:(NSDictionary*) data;
-(NSString*) GetCSharpParamType:(NSDictionary *)type;
-(NSString*) CToCSharpType:(NSString *)type;
-(NSString*) CSharpToCType:(NSString*) type withName:(NSString*) name;
-(NSString*) GetCSharpMarshallingReturnType:(NSDictionary*) data;
-(NSString*) CArgToCSharpArg: (NSDictionary*) data;
-(NSString*) GetObjCConverter: (NSDictionary*) data;
-(NSString*) CSharpArgToCArg: (NSString*) NativeType withName:(NSString*) ParamName;
-(GRMustacheTemplate*) CArgToObjCArg: (NSString*) NativeType;
-(NSString*) RenderBlockTemplate: (NSDictionary*) data;
-(void) AddConverters:(NSMutableDictionary*) dict;
-(NSString*) GetMarshallingAttributesForType:(NSString*) NativeType;
-(BOOL) IsStatic:(id)value;

-(NSString*) Capitalize:(NSString*) str;
@end

NS_ASSUME_NONNULL_END
