//
//  CallbackGenerator.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/20/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "CallbackGenerator.h"
#import "JSONLoader.h"

@implementation CallbackGenerator

-(id) init
{
    if(self = [super init])
    {
        converter = [[Converters alloc] init];
    }
    
    return self;
}

-(NSDictionary*) GenerateCallbacks
{
    NSString* filepath = @"./json/Data/BlockData.json";
    NSMutableDictionary* data = [[JSONLoader LoadJSONData:filepath] mutableCopy];
    return data;
}

@end
