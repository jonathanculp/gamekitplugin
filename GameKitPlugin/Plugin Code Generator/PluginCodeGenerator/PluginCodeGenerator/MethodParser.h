//
//  MethodParser.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/2/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MethodParser : NSObject
-(NSMutableDictionary*) ParseMethod: (NSString*) methodDecl;
@end

NS_ASSUME_NONNULL_END
