//
//  Converters.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/20/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "Converters.h"
#import "GRMustache.h"
#import "JSONLoader.h"
#import "Utils.h"

@implementation Converters

-(id) init {
    if(self = [super init])
    {
        BlockData = [JSONLoader LoadJSONData:@"./json/Data/BlockData.json"];
        BlockTemplate = [self LoadTemplate:@"ObjC/BlockTemplate"];
        ObjCPropertyBlockTemplate = [self LoadTemplate:@"ObjC/PropertyBlockTemplate"];
        CSharpBlockTemplate = [self LoadTemplate:@"CSharp/BlockTemplate"];
        CSharpPropertyBlockTemplate = [self LoadTemplate:@"CSharp/PropertyBlockTemplate"];
        
        NSArray* pointerTypes = [JSONLoader LoadJSONData:@"./json/Data/PointerTypes.json"];
        
        PointerTypes = [NSSet setWithArray:pointerTypes];
        
        NSArray* stringTypes = [JSONLoader LoadJSONData:@"./json/Data/StringTypes.json"];
        StringTypes = [NSSet setWithArray:stringTypes];
        
        NSArray* enumTypes = [JSONLoader LoadJSONData:@"./json/Data/EnumTypes.json"];
        EnumTypes = [NSSet setWithArray:enumTypes];
        
        DateTypes = [NSSet setWithObjects:
                     @"NSDate*",
                     @"NSTimeInterval",
                     nil];
        
        
        // Renders a function argument. You know like...
        // SomeFunc(ArgumentType ArgumentValue)
        // Most of our template rendering is argument conversions so it's
        // good to have this template cached
        
        ArgumentTemplate = [GRMustacheTemplate templateFromString:@"{{&Type}}{{#Name}} {{.}}{{/}}" error:nil];
    }
    
    return self;
}

/// Returns the C type for the corresponding Objective-C type. If there is a simple conversion between the two
-(NSString*) SimpleCType:(NSString*) type
{
    if([PointerTypes containsObject:type])
        return @"void*";
    else if([StringTypes containsObject:type])// || [Utils IsStringArray:type])
        return @"const char*";
    else if([DateTypes containsObject:type])
        return @"double";
    else if([EnumTypes containsObject:type] || [type isEqualToString:@"long"])
        return @"long";
    else if([type isEqualToString:@"id"])
        return @"void*";
    else if(type == NULL || [type isEqualToString:@"void"])
        return @"void";
    else if([type isEqualToString:@"BOOL"])
        return @"bool";
    else if([type isEqualToString:@"NSUInteger"])
        return @"uint";
    // Types that don't need to be converted to anything
    else if([type isEqualTo:@"double"] || [type isEqualTo:@"NSInteger"] || [type isEqualTo:@"float"] || [type isEqualTo:@"uint32_t"])
        return type;
    else
        return nil;
}

// returns the marshalling type (value returned to unity)
// for the given objective-c type

-(NSString*) ObjCToCType:(NSDictionary*) data
{
    NSString* type = data[@"Type"];
    NSError* anyError;
    NSString* templateStr;
    
    NSString* simpleType = [self SimpleCType:type];
    if(simpleType != nil)
    {
        NSMutableDictionary* d = [[NSMutableDictionary alloc] init];
        d[@"Type"] = simpleType;
        if(data[@"Name"] != nil)
            d[@"Name"] = data[@"Name"];
        
        return [ArgumentTemplate renderObject:d error:nil];
    }
    else if([Utils IsBlock:type]){
        NSString* newType = [type stringByReplacingOccurrencesOfString:@"Block" withString:@"Callback"];
        
        if([Utils IsMethodBlock:type])
        {
            return [NSString stringWithFormat:@"unsigned long invocationId, %@ %@", newType, data[@"Name"]];
        }
        
        return [NSString stringWithFormat:@"%@ %@", newType, data[@"Name"]];
    }
    else if([Utils IsStringArray:type])
        templateStr = @"const char* {{&Name}}[],\n\tlong {{&Name}}Count";
    else if([Utils IsArrayOfPointers:type] || [Utils IsUntypedArray:type])
        templateStr = @"void* {{&Name}}[],\n\tlong {{&Name}}Count";
    else if([Utils IsDictionary:type])
        templateStr = @"void* {{&Name}}Keys[], void* {{&Name}}Values[], long {{&Name}}Count";
    else if([type isEqualToString:@"NSData*"])
        templateStr = @"const void* {{&Name}}, long {{&Name}}Length";
    else
    {
        NSLog(@"encountered unsupported type: %@", type);
        return [NSString stringWithFormat:@"UNSUPPORTED_TYPE_%@", type];
    }

    GRMustacheTemplate* template = [GRMustacheTemplate templateFromString:templateStr error:&anyError];
    
    if(template == nil)
    {
        NSLog(@"%@", anyError);
    }
    
    NSString* contents = [template renderObject:data error:&anyError];
    if(contents == nil)
    {
        NSLog(@"%@", anyError);
    }
    
    return contents;
}

-(NSString*) GetCSharpParamType:(NSDictionary *)data
{
    if(data[@"CSharp"] != nil)
    {
        if(data[@"CSharp"][@"OverridePropertyType"])
        {
            return data[@"CSharp"][@"OverridePropertyType"];
        }
    }
    
    NSString* type = data[@"Type"];
    
    if([PointerTypes containsObject:type]){
        if([type isEqualToString:@"id<CKRecordValue>"])
            return @"UNSUPPORTED_TYPE";
        // everything but the '*'
        return [self RightTrim:type];
    }
    else if([StringTypes containsObject:type])
        return @"string";
    else if([DateTypes containsObject:type])
        return @"DateTime";
    else if([type isEqualTo:@"NSData*"])
        return @"byte[]";
    else if([Utils IsBlock:type])
        return [self BlockToGenericDelegate:type];
    else if([Utils IsStringArray:type])
        return @"string[]";
    else if([Utils IsArrayOfPointers:type])
        return [NSString stringWithFormat:@"%@[]", [self RightTrim:[Utils ArraySubType:type]]];
    else if([Utils IsUntypedArray:type])
        return @"object[]";
    else if([Utils IsDictionary:type]){
        NSString* keyType = [Utils DictionaryKeyType:type];
        NSString* valueType = [Utils DictionaryValueType:type];
        return [NSString stringWithFormat:@"Dictionary<%@,%@>",
                [self GetCSharpParamType:@{@"data":keyType}],
                [self GetCSharpParamType:@{@"data":valueType}]];
    }
    else if([type isEqualTo:@"NSInteger"] || [type isEqualToString:@"long"])
        return @"long";
    else if([type isEqualTo:@"BOOL"])
        return @"bool";
    else if([type isEqualTo:@"NSUInteger"])
        return @"ulong";
    else if([type isEqualTo:@"uint32_t"])
        return @"uint";
    else if([EnumTypes containsObject:type]
        || [type isEqual:@"float"]
        || [type isEqualTo:@"double"]
        || [type isEqualTo:@"void"]
        || [type isEqualTo:@"object"])
    {
        return type;
    }
    else if([type isEqualTo:@"void*"] ||
         [type isEqualTo:@"void**"])
    {
        return @"IntPtr";
    }
    else if([type isEqualTo:@"NSData*"])
        return @"byte[]";
    else if(type == NULL)
        return @"void";
    
    NSLog(@"encounter unsupported type %@", type);
    return @"UNSUPPORTED_TYPE";
}

-(NSString*) BlockArgs:(NSString*) NativeType
{
    NSError* anyError;
    
    NSMutableDictionary* blockData = [self GetBlockData:NativeType];
    [self AddConverters:blockData];
    
    NSString* templateStr = @"{{#each(Params)}}{{&CSharpParamType(Type)}}{{^@last}},{{/}}{{/}}";
    
    GRMustacheTemplate* template = [GRMustacheTemplate templateFromString:templateStr error:&anyError];
    NSString* str = [template renderObject:blockData error:&anyError];
    
    if(str == nil)
    {
        NSLog(@"%@", anyError);
    }
    
    return str;
}

-(NSString*) BlockToGenericDelegate:(NSString*) NativeType
{
    NSError* anyError;
    
    NSMutableDictionary* blockData = [self GetBlockData:NativeType];
    [self AddConverters:blockData];
    
    NSMutableArray* convertedParams = [[NSMutableArray alloc] init];
    NSString* returnType = blockData[@"ReturnType"];
    
    BOOL isVoidReturn = returnType == nil || [returnType isEqualToString:@"void"];
    NSString* delegateType = isVoidReturn ? @"Action" : @"Func";
    
    NSString* blockArgs = [self BlockArgs:NativeType];
    NSString* templateStr = [NSString stringWithFormat:@"%@<%@>", delegateType, blockArgs];
    
    GRMustacheTemplate* template = [GRMustacheTemplate templateFromString:templateStr error:nil];
    if(returnType != nil)
        [convertedParams addObject:[self CToCSharpType:returnType]];
                            
    NSString* str = [template renderObject:blockData error:&anyError];
    if(str == nil)
    {
        NSLog(@"%@", anyError);
    }
    return str;
}

-(NSMutableDictionary*) GetBlockData:(NSString *)blockName
{
    NSArray* blocks = BlockData[@"Blocks"];
    NSInteger idx = [blocks indexOfObjectPassingTest:^BOOL(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        return [BlockData[@"Blocks"][idx][@"Type"] isEqual:blockName];
    }];
    
    NSMutableDictionary* newBlockData = [blocks[idx] mutableCopy];
    [self AddConverters:newBlockData];
    return newBlockData;
}

-(void) AddConverters:(NSMutableDictionary*) dict
{
    dict[@"Date"] = [GRMustacheFilter filterWithBlock:^id(id value) {
    
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSDate *date  = [NSDate now];

        // Convert to new Date Format
        [dateFormatter setDateFormat:@"MM/dd/yyyy"];
        NSString *newDate = [dateFormatter stringFromDate:date];
        return newDate;
    }];
    
    dict[@"Year"] = [GRMustacheFilter filterWithBlock:^id(id value) {
    
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        NSDate *date  = [NSDate now];

        // Convert to new Date Format
        [dateFormatter setDateFormat:@"yyyy"];
        NSString *newDate = [dateFormatter stringFromDate:date];
        return newDate;
    }];
    
    dict[@"CToCSharp"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self CArgToCSharpArg:value];
    }];
    
    dict[@"CSharpToC"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
        
        NSString* type = arguments.count > 0 ? arguments[0] : nil;
        NSString* name = arguments.count > 1 ? arguments[1] : nil;
        return [self CSharpArgToCArg:type withName:name];
    }];
    
    dict[@"PropSetCSharpToC"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        NSString* type = value[@"Type"];
        NSString* name = value[@"Name"];
        
        return [self CSharpArgToCArg:type withName:@"value"];
    }];
    
    dict[@"CToCSharpType"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
        NSString* type = arguments.count > 0 ? arguments[0] : nil;
        return [self CToCSharpType:type];
    }];
    
    dict[@"CSharpToCType"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
        NSString* type = arguments.count > 0 ? arguments[0] : nil;
        NSString* name = arguments.count > 1 ? arguments[1] : nil;
        return [self CSharpToCType:type withName:name];
    }];
    
    dict[@"BlockArgs"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self BlockArgs:value];
    }];
    
    dict[@"CSharpParamType"] = [GRMustacheFilter
                                variadicFilterWithBlock:^id(NSArray *arguments) {
        id type = arguments.count > 0 ? arguments[0] : nil;
        if([type isKindOfClass:[NSDictionary class]])
            return [self GetCSharpParamType:type];
        else
            return [self GetCSharpParamType:@{@"Type":type}];
    }];
    
    dict[@"CSharpMarshallingType"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        
        if(value == nil)
            return @"BADFOOD";
        
        if([value isKindOfClass:[NSDictionary class]])
            return [self GetCSharpMarshallingReturnType:value];
        
        return [self GetCSharpMarshallingReturnType:@{@"Type":value}];
    }];
    
    dict[@"CSharpMarshallingType2"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetCSharpMarshallingReturnType:@{@"Type":value[@"Type"]}];
    }];
    
    dict[@"CToObjCConverter"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self CArgToObjCArg:value];
    }];
    
    dict[@"ObjCConverter"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverter:value];
    }];
    
    dict[@"ObjCConverter2"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverter:@{@"Type":value[@"Type"],@"Name":@"val"}];
    }];
    
    dict[@"CSharpConverterStart"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetCSharpConverterStart:@{@"Type":value[@"Type"],@"Name":value[@"Name"]}];
    }];
    
    dict[@"AsCallback"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [value stringByReplacingOccurrencesOfString:@"Block" withString:   @"Callback"];
    }];
    
    dict[@"ObjCToCType"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        if([value isKindOfClass:[NSDictionary class]])
            return [self ObjCToCType:value];
        return [self ObjCToCType:@{@"Type":value}];
    }];
    
    dict[@"OnlyCallbacks"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        NSArray* arr = value;
        NSPredicate *pred = [NSPredicate predicateWithFormat:@"(Type contains[c] %@)", @"Block"];
        NSArray *filtered = [arr filteredArrayUsingPredicate:pred];
        return filtered;
    }];
    
    dict[@"IsBlockType"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        NSString* Type = value;
        if([Utils IsBlock:value])
            return @{};
        return nil;
    }];
    
    dict[@"IsStatic"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self IsStatic:value] ? @{} : nil;
    }];
    
    dict[@"StripClassName"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
        NSString* className = arguments[0];
        NSString* name = arguments[1];
        
       // Sorta hacky, just chop off some number of characters
        long loc = [className length];
        long len = [name length] - loc;
        return [name substringWithRange:NSMakeRange(loc, len)];
    }];
    
    // Only converts the type, not the name
    // we leave the name out of the dict we pass in
    dict[@"ObjCToCType2"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self ObjCToCType:@{@"Type":value[@"Type"]}];
    }];
    
    dict[@"MarshallingAttributes"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetMarshallingAttributesForType:value];
    }];
    
    dict[@"Capitalize"] = [GRMustacheFilter filterWithBlock:^id(id object) {
        return [self Capitalize:object];
    }];
    
    dict[@"TrimNumeric"] = [GRMustacheFilter filterWithBlock:^id(id object) {
        NSString* string = object;
        NSCharacterSet* charSet = [NSCharacterSet decimalDigitCharacterSet];
        
        long i = [string length] - 1;
        
        if([charSet characterIsMember:[string characterAtIndex:i]] == NO)
            return string;
        
        while (i >= 0 && [charSet characterIsMember:[string characterAtIndex:i]])
        {
            i--;
        }
        
        return [string substringToIndex:i+1];
    }];
    
    dict[@"ArraySubtype"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self RightTrim:[Utils ArraySubType:value]];
    }];
    
    /// This is just a hack to keep track of the number of times we've seen
    /// an array parameter in the argument list. Some of the marshalling
    /// attributes need an index, and they get tricky to calculate when one parameter expands to
    /// two. Mustache templates don't have a great way of handling what I need. These tags are only
    /// really needed in the DLL section of the C# class template
    
    dict[@"MethodStart"] = [GRMustacheRendering renderingObjectWithBlock:^NSString *(GRMustacheTag *tag, GRMustacheContext *context, BOOL *HTMLSafe, NSError *__autoreleasing *error) {
        
        self->NumTimesSeenArrayParam = 0;
        
        // Use NumArgs as an offset if this is not a static method, to account for the mandatory this pointer parameter
        // (which won't be in the block data)
        self->NumArgs = [self IsStatic:[context valueForMustacheKey:@"MethodType"]] ? 0 : 1;
        return nil;
    }];
    
    dict[@"IncArg"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        self->NumArgs++;
        return nil;
    }];
    
    dict[@"IsReadonly"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        NSDictionary* param = value;
        NSArray* attributes = param[@"attributes"];
        
        if(attributes != nil && [attributes containsObject:@"readonly"])
            return @{};
        
        return nil;
    }];
    
    dict[@"IsWriteonly"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        NSDictionary* param = value;
        NSArray* attributes = param[@"attributes"];
        
        if(attributes != nil && [attributes containsObject:@"writeonly"])
            return @{};
        
        return nil;
    }];
    
    dict[@"IsNotVoid"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        if([value isEqualTo:@"void"])
            return nil;
        return @{};
    }];
    
    dict[@"Nonnull"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        if([[value objectForKey:@"Attr"] isEqualToString:@"nonnull"])
           return @{};
        return nil;
    }];
    
    dict[@"BlockRenderer"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
        NSString* methodName = arguments[0];
        NSString* blockType = arguments[1];
        NSDictionary* blockInfo = [self GetBlockData:blockType];
        NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
        
        data[@"MethodName"] = methodName;
        data[@"Block"] = blockInfo;
        [self AddConverters:data];
        
        NSString* render = [self->CSharpBlockTemplate renderObject:data error:nil];

        return render;
    }];
    
    dict[@"ObjCPropertyBlockRenderer"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
        NSError* anyError;
        NSString* blockType = arguments[2];
        
        NSDictionary* blockInfo = [self GetBlockData:blockType];
        NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
        [self AddConverters:data];
        
        data[@"ClassName"] = arguments[0];
        data[@"Prop"] = arguments[1];
        data[@"Block"] = blockInfo;
        NSString* render = [self->ObjCPropertyBlockTemplate renderObject:data error:&anyError];
        return render;
    }];
    
    dict[@"PropertyBlockRenderer"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray *arguments) {
        NSString* blockType = arguments[2];
        NSDictionary* blockInfo = [self GetBlockData:blockType];
        NSMutableDictionary* data = [[NSMutableDictionary alloc] init];
        
        data[@"ClassName"] = arguments[0];
        data[@"Prop"] = arguments[1];
        data[@"Block"] = blockInfo;
        [self AddConverters:data];
        
        NSString* render = [self->CSharpPropertyBlockTemplate renderObject:data error:nil];

        return render;
    }];
    
    dict[@"ObjCConverterStart"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverterStart:value];
    }];
    dict[@"ObjCConverter"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverter:value];
    }];
    dict[@"ObjCConverterEnd"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverterEnd:value];
    }];
    
    dict[@"DefaultValue"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetDefaultValueForType:value];
    }];
}

-(BOOL) IsStatic:(id) value
{
    // OH HELL, you can't access parent array element is GRMustance, so
    // this is here so that if you address them directly you can figure
    // out if they're static or not
    // FUCKING MUSTACHE
    // Hope this doesn't cause any bugs
    if([value isKindOfClass:[NSString class]])
    {
        return ([value isEqual:@"Class"] || [value isEqual:@"Static"]);
    }
    
    // Check for methods
    NSString* methodType = value[@"MethodType"];
    if(methodType != nil && [methodType isEqualToString:@"Class"])
        return YES;
    
    // Check for blocks
    NSString* isStatic = value[@"Static"];
    if(isStatic != nil && [isStatic isEqualToString:@"YES"])
        return YES;
    
    // The check for properties & notifications
    NSArray* propertyAttributes = value[@"attributes"];
    if(propertyAttributes != nil && ([propertyAttributes containsObject:@"class"] || [propertyAttributes containsObject:@"static"]))
        return YES;
    
    return NO;
}

-(NSString*) GetMarshallingAttributesForType:(NSDictionary *)Param
{
    NSString* type = Param[@"Type"];
    NSNumber* index = Param[@"Index"];
    NSAssert(index != nil, @"Missing Required Parameter Index");
    
    int i = [index intValue];
    
    if([Utils IsArrayOfPointers:type])
    {
        // To marshal an array, we expand the array parameter into a C array
        // and a count parameter. This attirbute tells the marshaller that
        // the size of the array comes after the current parameter
        NumTimesSeenArrayParam++;
        
        // + 2 for the two literal arguments to the callback
        // 1 is the "this" pointer for the object that owns the callback
        // 2 is the invocation Id which identifies the actual function instance
        
        return [NSString stringWithFormat:@"[MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.SysInt, SizeParamIndex = %i)]", i + NumTimesSeenArrayParam + NumArgs];
    }
    else if([Utils IsStringArray:type])
    {
        NumTimesSeenArrayParam++;
        return [NSString stringWithFormat:@"[MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPStr, SizeParamIndex = %i)]", i + NumTimesSeenArrayParam + NumArgs];
    }
//    else if([Utils IsDictionary:type])
//    {
//        // To marshal an array, we expand the array parameter into a C array
//        // and a count parameter. This attirbute tells the marshaller that
//        // the size of the array comes after the current parameter
//        NumTimesSeenArrayParam += 2;
//
//        return [NSString stringWithFormat:@"[MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.SysInt, SizeParamIndex = %i)]", i + NumTimesSeenArrayParam + NumArgs + 1];
//    }
    
    return nil;
}

// returns the C# type that we will convert the marshalled type
// to on the unity side of things

-(NSString*) CToCSharpType:(NSString *)type
{
    if([PointerTypes containsObject:type]){
        if([type isEqualToString:@"instancetype"])
            return @"UNSUPPORTED_TYPE";
        // everything but the '*'
        return [self RightTrim:type];
    }
    else if([type isEqualTo:@"NSInteger"])
    {
        return @"long";
    }
    else if([StringTypes containsObject:type])
        return @"string";
    else if([Utils IsDictionary:type])
    {
        NSString* type1 = [Utils DictionaryKeyType:type];
        NSString* type2 = [Utils DictionaryValueType:type];
        return [NSString stringWithFormat:@"Dictionary<%@,%@>", [self CToCSharpType:type1], [self CToCSharpType:type2]];
    }
    else if([Utils IsStringArray:type])
        return @"string[]";
    else if([Utils IsArray:type])
        return [NSString stringWithFormat:@"%@[]", [self RightTrim:[Utils ArraySubType:type]]];
    else if([DateTypes containsObject:type])
        return @"DateTime";
    else if([Utils IsBlock:type])
        return [type stringByReplacingOccurrencesOfString:@"Block" withString:@"Callback"];
    else if([type isEqualTo:@"BOOL"])
        return @"bool";
    else if(type == NULL)
        return @"void";
    else if([type isEqualTo:@"NSData*"])
        return @"byte[]";
    
    return type;
}

-(NSString*) CSharpToCType:(NSString*) type
                  withName:(NSString*) name
{
    NSError* anyError;
    NSString* templateStr;
    
    if([PointerTypes containsObject:type])
        templateStr = @"IntPtr{{#Name}} {{&.}}{{/}}";
//        templateStr = @"HandleRef{{#Name}} {{&.}}{{/}}";
    else if([StringTypes containsObject:type])
        templateStr = @"string{{#Name}} {{&.}}{{/}}";
    else if([DateTypes containsObject:type])
        templateStr = @"double{{#Name}} {{&.}}{{/}}";
    else if([EnumTypes containsObject:type])
        templateStr = @"long{{#Name}} {{&.}}{{/}}";
    else if([Utils IsBlock:type]){
        NSString* t = [type stringByReplacingOccurrencesOfString:@"Block" withString:@"Delegate"];
        
        if([Utils IsMethodBlock:type]){
            return [NSString stringWithFormat:@"ulong invocationId, %@ %@", t, name == nil ? @"" : name];
        }
        
        return [NSString stringWithFormat:@"%@ %@", t, name == nil ? @"" : name];
    }
    else if([Utils IsStringArray:type])
        templateStr = @"string[] {{&Name}},\n\t\t\tint {{&Name}}Count";
    else if([Utils IsArrayOfPointers:type]){
        templateStr = @"IntPtr[] {{&Name}},\n\t\t\tint {{&Name}}Count";
    }
    else if(type == NULL)
        return @"void";
    else if([type isEqualTo:@"double"])
        templateStr = @"double{{#Name}} {{&.}}{{/}}";
    // types that don't need converting
    else if([type isEqualTo:@"float"])
        templateStr = [NSString stringWithFormat:@"%@{{#Name}} {{&.}}{{/}}", type];
    else if([type isEqualTo:@"NSInteger"] || [type isEqualToString:@"long"])
        templateStr = @"long{{#Name}} {{&.}}{{/}}";
    else if([type isEqualToString:@"NSUInteger"])
        templateStr = @"ulong{{#Name}} {{&.}}{{/}}";
    else if([type isEqualTo:@"BOOL"])
        templateStr = @"bool{{#Name}} {{&.}}{{/}}";
    else if([type isEqualTo:@"uint32_t"])
        templateStr = @"uint{{#Name}} {{&.}}{{/}}";
    else if([type isEqualTo:@"void*"] || [type isEqualTo:@"void**"])
        templateStr = @"IntPtr{{#Name}} {{&.}}{{/}}";
    else if([type isEqualTo:@"NSArray*"])
        templateStr = @"object[]{{#Name}} {{&.}}{{/}}";
    else if([type isEqualTo:@"NSData*"])
        templateStr = @"byte[] {{&Name}},\n\t\t\tint {{&Name}}Length";
    else
    {
        NSLog(@"Encountered unsupported type %@", type);
        return @"UNSUPPORTED_TYPE";
    }
    
    if(templateStr != nil)
    {
        if([name isEqual:@"delegate"])
            name = @"del";
        
        GRMustacheTemplate* template = [GRMustacheTemplate templateFromString:templateStr error:&anyError];
        NSString* str = [template renderObject:@{@"Name":name, @"Type":type} error:&anyError];
        return str;
    }
    
    return type;
}

/// if the type is simple to convert (does not require expansion like arrays
/// do) then this will convert the Objective-C type to it's corresponding marshalling type
/// returns null if the type is unknown or not supported

-(NSString*) ToMarshallingReturnType:(NSString*) type
{
    if([PointerTypes containsObject:type] || [type isEqualToString:@"id"])
        return @"IntPtr";
    else if([StringTypes containsObject:type] || [StringArrayTypes containsObject:type])
        return @"IntPtr";
    else if([DateTypes containsObject:type])
        return @"double";
    else if([type isEqualTo:@"NSInteger"] || [type isEqualToString:@"long"])
        return @"long";
    else if([type isEqualTo:@"NSUInteger"])
        return @"ulong";
    else if([type isEqualTo:@"uint32_t"])
        return @"uint";
    else if([type isEqualTo:@"BOOL"])
        return @"bool";
    else if([Utils IsBlock:type]){
        NSString* newType = [type stringByReplacingOccurrencesOfString:@"Block" withString:@"Delegate"];
        return [self Capitalize:newType];
    }
    
    // Types that don't need converting...
    else if([EnumTypes containsObject:type]
            || [type isEqualTo:@"double"]
            || [type isEqualTo:@"void"]
            || [type isEqualTo:@"float"]
            )
    {
        return type;
    }
    else if([type isEqualTo:@"NSInteger"])
    {
        return @"long";
    }
    
    return nil;
}

-(NSString*) GetCSharpMarshallingReturnType:(NSDictionary*) data;
{
    //NSArray* name = data[@"Name"];
    NSString* name = data[@"Name"];
    
    if([name isEqual:@"delegate"])
        name = @"del";
    
    NSString* simpleType = [self ToMarshallingReturnType:data[@"Type"]];
    
    // Check for a simple conversion from the Objective-C type
    // to the marshalling type. Most things are simple conversions
    
    if(simpleType != nil)
    {
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        dict[@"Type"] = simpleType;
        if(name != nil)
            dict[@"Name"] = name;
        
        return [ArgumentTemplate renderObject:dict error:nil];
    }
    
    // if there is no simple conversion to the marshalling type...
    // check for one of the advanced types
    
    else
    {
        NSString* templateStr;
        NSString* type = data[@"Type"];
        
        if([Utils IsArrayOfPointers:type] || [Utils IsStringArray:type])
        {
            templateStr = [NSString stringWithFormat:@"IntPtr[] {{&Name}},\n\t\tlong {{&Name}}Count"];
        }
        else if([Utils IsDictionary:type])
        {
            templateStr = [NSString stringWithFormat:@"[MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.SysInt, SizeParamIndex = 3)]\n\t\tIntPtr[] {{&Name}}Keys,\n\t\t[MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.SysInt, SizeParamIndex = 3)]\n\t\tIntPtr[] {{&Name}}Values,\n\t\tlong {{&Name}}Count"];
        }
        else if([type isEqualTo:@"NSData*"])
        {
            templateStr = @"IntPtr {{&Name}}, long {{&Name}}Length";
        }
        else
        {
            templateStr = @"UNSUPPORTED_TYPE{{#Name}} {{.}}{{/}}";
        }
        
        GRMustacheTemplate* template = [GRMustacheTemplate templateFromString:templateStr error:nil];
        
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        dict[@"Type"] = data[@"Type"];
        if(name != nil)
            dict[@"Name"] = name;
        
        return [template renderObject:dict error:nil];
    }
}

-(NSString*) CArgToCSharpArg: (NSDictionary*) data
{
    
    NSString* NativeType = data[@"Type"];
    NSAssert(NativeType != nil, @"Type is nil");
    
    NSString* Name = data[@"Name"] == nil ? @"val" : data[@"Name"];
    
    if([Name isEqual:@"delegate"])
        Name = @"del";
    
    if(data[@"CSharp"] != nil)
    {
        NSString* overrideType = data[@"CSharp"][@"OverridePropertyType"];
        if(overrideType != nil)
        {
            return [NSString stringWithFormat:@"new %@(%@)", overrideType, Name];
        }
    }
    
    if([StringTypes containsObject:NativeType]){
        return [NSString stringWithFormat:@"Marshal.PtrToStringAuto(%@)", Name];
    }
//    else if([StringArrayTypes containsObject:NativeType]){
//        return [NSString stringWithFormat:@"Marshal.PtrToStringAuto(%@).Split(',').ToArray()", Name];
//    }
    else if([Utils IsStringArray:NativeType])
    {
        NSString* str = [NSString stringWithFormat:@"%@ == null ? null : %@.Select(x => Marshal.PtrToStringAuto(x)).ToArray()", Name, Name];
        return str;
    }
    else if([PointerTypes containsObject:NativeType]){
        return [NSString stringWithFormat:@"%@ == IntPtr.Zero ? null : new %@(%@)", Name, [self CToCSharpType: NativeType], Name];
    }
    else if([DateTypes containsObject:NativeType]){
        return [NSString stringWithFormat:@"new DateTime(1970, 1, 1, 0, 0, 0,DateTimeKind.Utc).AddSeconds(%@);", Name];
    }
    else if([EnumTypes containsObject:NativeType])
    {
        return [NSString stringWithFormat:@"(%@) %@", NativeType, Name];
    }
    else if([NativeType isEqualTo:@"NSData*"])
    {
        return [NSString stringWithFormat:@"%@Bytes", Name];
    }
    else if([Utils IsArrayOfPointers:NativeType])
    {
        // Here we only return the array, it's been marshalled back to C#
        // with the correct size and we can ditch the count paramater
        NSString* subtype = [self RightTrim:[Utils ArraySubType:NativeType]];
        NSString* str = [NSString stringWithFormat:@"%@ == null ? null : %@.Select(x => new %@(x)).ToArray()", Name, Name, subtype];
        return str;
    }
    else if([Utils IsDictionary:NativeType])
    {
        NSString* Type1 = [self CArgToCSharpArg:@{@"Type":[Utils DictionaryKeyType:NativeType], @"Name":@"k"}];
        NSString* Type2 = [self CArgToCSharpArg:@{@"Type":[Utils DictionaryValueType:NativeType], @"Name":@"v"}];
        return [NSString stringWithFormat:@"%@Keys.Zip(%@Values, (k, v) => ( %@, %@ )).ToDictionary(item => item.Item1, item => item.Item2)", Name, Name, Type1, Type2];
    }
    else
        return Name;
}


// returns the code converts an objective-c type to the type
// it needs to be to marshall up to unity.
-(NSString*) GetObjCConverter: (NSDictionary*) data
{
    NSString* templateString = @"%@";
    NSString* NativeType = data[@"Type"];
    NSString* paramName = data[@"Name"];
    
    if([StringTypes containsObject:NativeType]){
        templateString = @"[%@ UTF8String]";
    }
    else if([PointerTypes containsObject:NativeType] || [NativeType isEqualToString:@"id"]){
        templateString = @"(__bridge_retained void*) %@";
    }
    else if([DateTypes containsObject:NativeType]){
        templateString = @"[%@ timeIntervalSince1970]";
    }
    else if([Utils IsArray:NativeType] || [Utils IsUntypedArray:NativeType])
    {
        return [NSString stringWithFormat:@"%@Buffer, %@Count", paramName, paramName];
    }
    else if([Utils IsDictionary:NativeType])
    {
        return [NSString stringWithFormat:@"%@Keys, %@Values, %@Count", paramName, paramName, paramName];
    }
    else if([NativeType isEqual:@"NSData*"])
    {
        return [NSString stringWithFormat:@"[%@ bytes], [%@ length]", paramName, paramName];
    }

    return [NSString stringWithFormat:templateString, paramName];
}

-(NSString*) GetObjCConverterStart:(NSDictionary *)data
{
    NSString* type = data[@"Type"];
    NSString* templateStr;
    GRMustacheTemplate* template;
    
    if([Utils IsArray:type])
    {
        //NSString* subtype = [Utils ArraySubType:type];
        
        if([Utils IsStringArray:type])
        {
            templateStr = @""
            "long {{&Name}}Count = [{{&Name}} count];\n"
            "\t\t\tconst char** {{&Name}}Buffer = [Converters NSArrayOfStringsToCArrayOfStringPointers:{{&Name}}];";
        }
        else
        {
            templateStr = @""
            "long {{&Name}}Count = [{{&Name}} count];\n"
            "\t\t\tvoid** {{&Name}}Buffer = nil;\n"
            "\t\t\tif({{&Name}}Count > 0)\n"
            "\t\t\t{\n"
            "\t\t\t\t{{&Name}}Buffer = (void**) malloc(sizeof(void*) * {{&Name}}Count);\n"
            "\t\t\t\t[Converters NSArrayToRetainedCArray:{{&Name}} withBuffer:{{&Name}}Buffer];\n"
            "\t\t\t}";
        }
        
        template = [GRMustacheTemplate templateFromString:templateStr error:nil];
        
    }
    else if([Utils IsDictionary:type])
    {
        templateStr = @"long {{&Name}}Count = [{{&Name}} count];\n"
            "\t\t\t\tvoid** {{&Name}}Keys = [Converters NSDictionaryToKeysArray:{{&Name}}];\n"
            "\t\t\t\tvoid** {{&Name}}Values = [Converters NSDictionaryToValuesArray:{{&Name}}];\n";
        template = [GRMustacheTemplate templateFromString:templateStr error:nil];
    }
    
    if(template != nil)
    {
        return [template renderObject:data error:nil];
    }
    
    return @"";
}

-(NSString*) GetObjCConverterEnd:(NSDictionary *)data
{
    NSString* type = data[@"Type"];
    NSString* name = data[@"Name"];
    
    if([Utils IsArray:type])
    {
        return [NSString stringWithFormat:@"free(%@Buffer);", name];
    }
    else if([Utils IsDictionary:type])
    {
        return [NSString stringWithFormat:@"free(%@Keys);\n\t\t\t\tfree(%@Values);", name, name];
    }
    return @"";
}

-(NSString*) GetCSharpConverterStart:(NSDictionary *)data
{
    NSString* type = data[@"Type"];
    NSString* name = data[@"Name"];
    
    if([type isEqualTo:@"NSData*"])
    {
        return [NSString stringWithFormat:@"byte[] %@Bytes = new byte[%@Length];\n\t\t\t\tif(%@ != IntPtr.Zero)\n\t\t\t\t\tMarshal.Copy(%@, %@Bytes, 0, (int) %@Length);", name, name, name, name, name, name];
    }
    
    return nil;
}

-(NSString*) GetDefaultValueForType:(NSString*) type
{
    if([type isEqualToString:@"BOOL"])
    {
        return @"NO";
    }
    else if([type isEqualToString:@"double"] || [type isEqualToString:@"NSDate*"])
    {
        return @"0.0";
    }
    else if([type isEqualToString:@"int"]
            || [type isEqualToString:@"long"]
            || [type isEqualTo:@"NSUInteger"]
            || [type isEqualTo:@"NSInteger"]
            || [EnumTypes containsObject:type])
    {
        return @"0";
    }
    
    return @"nil";
}

-(id) CSharpArgToCArg: (NSString*) NativeType withName:(NSString*) ParamName
{
    NSError* anyError;
    
    if([PointerTypes containsObject:NativeType])
        return [NSString stringWithFormat:@"%@ != null ? HandleRef.ToIntPtr(%@.Handle) : IntPtr.Zero", ParamName, ParamName];
//        return [NSString stringWithFormat:@"%@.Handle", ParamName];
    
    if([Utils IsStringArray:NativeType] || [Utils IsUntypedArray:NativeType])
        return [NSString stringWithFormat:@"%@,\n\t\t\t\t%@?.Length ?? 0", ParamName, ParamName];
    
    if([Utils IsArrayOfPointers:NativeType] || [Utils IsUntypedArray:NativeType])
        return [NSString stringWithFormat:@"%@ == null ? null : %@.Select(x => HandleRef.ToIntPtr(x.Handle)).ToArray(),\n\t\t\t\t%@ == null ? 0 : %@.Length", ParamName, ParamName, ParamName, ParamName];
    
    if([EnumTypes containsObject:NativeType])
        return [NSString stringWithFormat:@"(long) %@", ParamName];
    
    if([Utils IsBlock:NativeType])
    {
        // iOS only supports marshalling function pointers as static methods
        // so we just pass in the name of a static method that's generated somewhere
        // else in the template
        
        GRMustacheTemplate* template = [GRMustacheTemplate templateFromString:@"{{Name}}Call.id, {{Capitalize(MethodName)}}Callback" error:&anyError];
        return template;
    }
    
    return ParamName;
}

-(NSString*) RenderBlockTemplate:(NSDictionary*) data
{
    // BARF - a template that renders a template
    NSError* anyError;
    NSString* NativeType = data[@"Type"];
    
    NSMutableDictionary* block = [[self GetBlockData:NativeType] mutableCopy];
    block[@"BlockName"] = data[@"Name"];

    block[@"ObjCConverterStart"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverterStart:value];
    }];
    block[@"ObjCConverter"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverter:value];
    }];
    block[@"ObjCConverterEnd"] = [GRMustacheFilter filterWithBlock:^id(id value) {
        return [self GetObjCConverterEnd:value];
    }];
    
    NSString* str = [BlockTemplate renderObject:block error:&anyError];
    
    if(str == NULL)
    {
        NSLog(@"Error rendering block template: %@", anyError);
    }
    
    return str;
}

-(NSString*) RightTrim:(NSString*) str
{
    return [str substringToIndex:[str length] - 1];
}

-(NSString*) LeftTrim:(NSString*) str
{
    return @"";
    //return [str substringToIndex:[str length] - 1];
}

-(NSString*) CArgToObjCArg: (NSDictionary*) data
{
    NSString* NativeType = data[@"Type"];
    NSString* str;

    if([StringTypes containsObject:NativeType])
    {
        str = @"[NSString stringWithUTF8String:{{&Name}}]";
    }
    else if([Utils IsStringArray:NativeType])
    {
        str = @"[Converters StringArray:{{&Name}} withCount:{{&Name}}Count]";
    }
    else if([PointerTypes containsObject:NativeType])
    {
        str = @"(__bridge {{{Type}}}) {{Name}}";
    }
    else if([EnumTypes containsObject:NativeType])
    {
        str = @"({{&Type}}){{&Name}}";
    }
    else if([Utils IsBlock:NativeType])
    {
        str = [self RenderBlockTemplate:data];
    }
//    else if([NativeType isEqualTo:@"double"])
//    {
//        str = @"[NSNumber numberWithDouble:{{&Name}}]";
//    }
    else if([NativeType isEqualTo:@"NSInteger"])
    {
        str = @"[NSNumber numberWithInteger:{{&Name}}]";
    }
    else if([Utils IsArrayOfPointers:NativeType] || [Utils IsUntypedArray:NativeType])
    {
        str = @"[Converters BridgedArray:{{&Name}} withCount:{{&Name}}Count]";
    }
    else
    {
        str = @"{{&Name}}";
    }
    
    GRMustacheTemplate* template = [GRMustacheTemplate templateFromString:str error:NULL];
    
    return [template renderObject:data error:nil];
}

-(GRMustacheTemplate*) LoadTemplate: (NSString*) filename
{
    NSError* anyError;
    NSString* filepath = [NSString stringWithFormat:@"./Templates/%@.mustache", filename];
    
    GRMustacheTemplate* template = [GRMustacheTemplate templateFromContentsOfFile:filepath error:&anyError];
    
    //NSAssert(template != nil, @"Failed to load template");
    if(template == nil)
    {
        NSLog(@"%@", anyError);
    }
    
    return template;
}

-(NSString*) Capitalize:(NSString *)input
{
    if([input length] == 0)
        return @"";

    /* get first char */
    NSString *firstChar = [input substringToIndex:1];

    /* create the new string */
    return [[firstChar uppercaseString] stringByAppendingString:[input substringFromIndex:1]];
}

@end
