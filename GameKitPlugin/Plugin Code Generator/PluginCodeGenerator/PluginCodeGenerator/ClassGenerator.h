//
//  ClassGenerator.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/2/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRMustache.h"
#import "Converters.h"

NS_ASSUME_NONNULL_BEGIN

@interface ClassGenerator : NSObject
{
    GRMustacheTemplate* blockTemplate;
    Converters* converter;
}

-(NSDictionary*) MakeData: (NSDictionary*) data;

-(NSMutableArray*) ParseProperties: (NSArray*) declarations;
-(NSMutableArray*) ParseMethods: (NSArray*) methods;

-(NSMutableDictionary*) AddMethodTemplateData: (NSDictionary *)data;

-(NSString*) GetObjCConverter:(NSString *) type;
-(NSString*) GetObjCConverterStart:(NSString *) type;
-(NSString*) GetObjCConverterEnd:(NSString *) type;
-(NSDictionary*) GetBlockData:(NSString *) blockName;

-(GRMustacheTemplate*) LoadTemplate:(NSString*) filename;
-(GRMustacheTemplate*) GetObjCTemplateForReturnType: (NSDictionary*) methodData;
-(GRMustacheTemplate*) GetCSharpTemplateForReturnType: (NSDictionary*) methodData;
-(GRMustacheTemplate*) GetCSharpDllTemplateForReturnType: (NSDictionary*) methodData;
-(GRMustacheTemplate*) GetMethodHeaderTemplateForType: (NSString*) nativeType;
@end

NS_ASSUME_NONNULL_END
