//
//  MethodParser.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/1/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tokenizer.h"
#import "Token.h"

NS_ASSUME_NONNULL_BEGIN

@interface MethodTokenizer : Tokenizer
{
}
-(Token*) Next;
-(NSString*) ParseIdentifier;
-(Token*) ParseReturnType;
@end

NS_ASSUME_NONNULL_END
