//
//  PropertyParser.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PropertyParser : NSObject
-(NSMutableDictionary*) ParseProperty:(NSString *) decl WithIndex:(int) index;
@end

NS_ASSUME_NONNULL_END
