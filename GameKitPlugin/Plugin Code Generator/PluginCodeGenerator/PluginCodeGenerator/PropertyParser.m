//
//  PropertyParser.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "PropertyParser.h"
#import "PropertyTokenizer.h"

@implementation PropertyParser
-(NSMutableDictionary*) ParseProperty: (NSString *) decl
                            WithIndex: (int)index
{
    NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
    PropertyTokenizer* tokenizer = [[PropertyTokenizer alloc] initWithString:decl];
    
    Token* token = NULL;
    
    NSMutableArray* attributes = [[NSMutableArray alloc] init];
    do
    {
        token = [tokenizer Next];
        
        NSString* tokenValue = [token Value];
        
        if([[token Type] isEqual:@"PropertyAttribute"])
        {
            // If there's a custom getter attribute, make sure we capture the custom name
            if([tokenValue hasPrefix:@"getter"])
            {
                NSArray *listItems = [tokenValue componentsSeparatedByString:@"="];
                data[@"Getter"] = listItems[1];
            }
            
            [attributes addObject:tokenValue];
        }
        else if([[token Type] isEqual:@"Type"])
        {
            data[@"Type"] = tokenValue;
        }
        else if([[token Type] isEqual:@"TypeAttribute"])
        {
            data[@"TypeAttr"] = tokenValue;
        }
        else if([[token Type] isEqual:@"PropertyName"])
        {
            data[@"Name"] = tokenValue;
        }
    }
    while( token != NULL );
    
    // If no getter name was specified - set it
    if(data[@"Getter"] == nil)
        data[@"Getter"] = data[@"Name"];
    
    data[@"attributes"] = attributes;
    data[@"Index"] = [NSNumber numberWithInt:index];
    return data;
}
@end
