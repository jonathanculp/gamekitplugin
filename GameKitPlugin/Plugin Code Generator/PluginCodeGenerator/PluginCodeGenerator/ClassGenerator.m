//
//  ClassGenerator.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/2/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "ClassGenerator.h"
#import "MethodParser.h"
#import "PropertyParser.h"
#import "JSONLoader.h"
#import "Utils.h"

@implementation ClassGenerator
-(id) init {
    
    if(self = [super init])
    {
        converter = [[Converters alloc] init];
    }
    
    return self;
}

-(NSDictionary*) MakeData: (NSDictionary*) classData
{
    NSMutableDictionary* data = [classData mutableCopy];
    
    NSMutableArray* ClassMethodList = [self ParseMethods:classData[@"ClassMethods"]];
    
    NSMutableArray* InstanceMethodList = [self ParseMethods:classData[@"InstanceMethods"]];
    [InstanceMethodList addObjectsFromArray:[self ParseMethods:classData[@"Methods"]]];
    
    NSMutableArray* InitMethodList = [self ParseMethods:classData[@"InitMethods"]];
    NSMutableArray* VoidMethodList = [self ParseMethods:classData[@"VoidMethods"]];
    NSMutableArray* PropertiesList = [self ParseProperties:classData[@"Properties"]];
    
    data[@"Plugin"] = @"_plugin";
    data[@"ClassMethods"] = ClassMethodList;
    data[@"InstanceMethods"] = InstanceMethodList;
    data[@"VoidMethods"] = VoidMethodList;
    data[@"InitMethods"] = InitMethodList;
    data[@"Properties"] = PropertiesList;
    data[@"Logging"] = @"YES";
    data[@"RemoveLastChar"] = [GRMustacheFilter variadicFilterWithBlock:^id(NSArray* args) {
            NSString* str = args[0];
            return [str substringToIndex:[str length] - 1];
    }];
    
    [converter AddConverters:data];
         
    return data;
}

-(NSMutableArray*) ParseProperties: (NSArray*) properties
{
    NSMutableArray* propertyList = [[NSMutableArray alloc] init];
    PropertyParser* parser = [[PropertyParser alloc] init];
    
    int count = 0;
    for(id property in properties)
    {
        // So originally I had all the method declarations and strings
        // in an array
        if([property isKindOfClass:[NSString class]])
        {
            NSMutableDictionary* propertyInfo = [[parser ParseProperty:property WithIndex:count++] mutableCopy];
            [self AddPropertyTemplateData:propertyInfo];
            [propertyList addObject:propertyInfo];
        }
        
        // But later I needed more structured data. And I didn't want
        // to re-write all the json files
        else if([property isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary* propertyInfo = [[parser ParseProperty:property[@"Declaration"] WithIndex:count++] mutableCopy];
            [self AddPropertyTemplateData:propertyInfo];
            [propertyInfo addEntriesFromDictionary:property];
            [propertyList addObject:propertyInfo];
        }
    }
    
    return propertyList;
}

-(NSMutableArray*) ParseMethods: (NSArray*) methods
{
    MethodParser* generator = [[MethodParser alloc] init];
    NSMutableArray* MethodList = [[NSMutableArray alloc] init];
    
    for (id method in methods)
    {
        // So originally I had all the method declarations and strings
        // in an array
        if([method isKindOfClass:[NSString class]])
        {
            NSDictionary* methodInfo = [generator ParseMethod:method];
            methodInfo = [self AddMethodTemplateData:methodInfo];
            [MethodList addObject:methodInfo];
        }
        
        // But later I needed more structured data. And I didn't want
        // to re-write all the json files
        else if([method isKindOfClass:[NSDictionary class]])
        {
            id methodDecl = method[@"Declaration"];            
            NSMutableDictionary* methodInfo = [generator ParseMethod:[methodDecl mutableCopy]];
            methodInfo = [self AddMethodTemplateData:methodInfo];
            [methodInfo addEntriesFromDictionary:method];
            [MethodList addObject:methodInfo];
        }
    }
    
    return MethodList;
}

-(NSDictionary*) AddMethodTemplateData:(NSDictionary*)data
{
    NSMutableDictionary* cpy = [data mutableCopy];
    
    cpy[@"Template"] = [self GetObjCTemplateForReturnType:data isHeader:NO isMethod:YES];
    cpy[@"ObjCHeaderTemplate"] = [self GetObjCTemplateForReturnType:data isHeader:YES isMethod:YES];
    cpy[@"CSharpTemplate"] = [self GetCSharpTemplateForReturnType:data isMethod:YES isDll:NO];
    cpy[@"CSharpDllTemplate"] = [self GetCSharpTemplateForReturnType:data isMethod:YES isDll:YES];
    
    NSArray* params = cpy[@"Parameters"];
    for(int i = 0; i < params.count; i++)
    {
        NSMutableDictionary* parameterInfo = params[i];
        
        // swap out reserved words
        if([parameterInfo[@"Name"] isEqual:@"object"])
            parameterInfo[@"Name"] = @"obj";
        
        if([parameterInfo[@"Name"] isEqual:@"delegate"])
            parameterInfo[@"Name"] = @"del";
    }
    return cpy;
}

-(NSDictionary*) AddPropertyTemplateData:(NSMutableDictionary*)cpy
{
    cpy[@"Template"] = [self GetObjCTemplateForReturnType:cpy isHeader:NO isMethod:NO];
    cpy[@"ObjCHeaderTemplate"] = [self GetObjCTemplateForReturnType:cpy isHeader:YES isMethod:NO];
    cpy[@"CSharpTemplate"] = [self GetCSharpTemplateForReturnType:cpy isMethod:NO isDll:NO];
    cpy[@"CSharpDllTemplate"] = [self GetCSharpTemplateForReturnType:cpy isMethod:NO isDll:YES];
    
    NSArray* params = cpy[@"Parameters"];
    for(int i = 0; i < params.count; i++)
    {
        NSMutableDictionary* parameterInfo = params[i];
        
        // Object is a reserved word in c#
        if([parameterInfo[@"Name"] isEqual:@"object"])
            parameterInfo[@"Name"] = @"obj";
    }
    return cpy;
}

-(GRMustacheTemplate*) LoadTemplate: (NSString*) filename
{
    NSError* anyError;
    NSString* filepath = [NSString stringWithFormat:@"./Templates/%@.mustache", filename];
    
    GRMustacheTemplate* template = [GRMustacheTemplate templateFromContentsOfFile:filepath error:&anyError];
    
    if(template == NULL)
    {
        NSLog(@"%@", anyError);
    }
    
    return template;
}

/// Returns an objective-c template for a particular return type. It's not exactly easy or readable to create one template
/// that can work with all return types. especially when you have complicated type conversions to do

-(GRMustacheTemplate*) GetObjCTemplateForReturnType: (NSDictionary*) data isHeader:(BOOL) isHeader isMethod:(BOOL)isMethod
{
    NSString* templateType = nil;
    NSString* returnType = isMethod ? data[@"ReturnParam"][@"Type"] : data[@"Type"];
    
    if([Utils IsStringArray:returnType])
    {
        templateType = @"StringArray";
    }
    else if([Utils IsArrayOfPointers:returnType])
    {
        templateType = @"ObjectArray";
    }
    else if([Utils IsUntypedArray:returnType])
    {
        templateType = @"UntypedArray";
    }
    else if(isMethod == NO && [Utils IsBlock:returnType])
    {
        templateType = @"Block";
    }
    else if([Utils IsInstanceType:returnType])
    {
        templateType = @"Init";
    }
    else
    {
        templateType = @"Basic";
    }
    
    NSString* methodType = data[@"MethodType"];
    NSString* templateName = [NSString stringWithFormat:@"ObjC/%@%@%@%@",
                              isHeader ? @"Header" : @"",
                              [methodType isEqual:@"Class"] ? @"Class" : @"",
                              isMethod ? @"Method" : @"Property",
                              templateType];
    
    GRMustacheTemplate* template = [self LoadTemplate:templateName];
    
    return template;
}

-(GRMustacheTemplate*) GetCSharpTemplateForReturnType: (NSDictionary*) data
                                             isMethod:(BOOL) isMethod
                                                isDll:(BOOL) isDll
{
    NSString* templateType = nil;
    NSString* returnType = isMethod ? data[@"ReturnParam"][@"Type"] : data[@"Type"];
    
    if([Utils IsStringArray:returnType])
    {
        templateType = @"StringArray";
    }
    else if([Utils IsArrayOfPointers:returnType])
    {
        templateType = @"ObjectArray";
    }
    else if([Utils IsUntypedArray:returnType])
    {
        templateType = @"UntypedArray";
    }
    else if([Utils IsDictionary:returnType])
    {
        templateType = @"Dictionary";
    }
    else if(isMethod == NO && [Utils IsBlock:returnType])
    {
        templateType = @"Block";
    }
    else
    {
        templateType = @"Basic";
    }
    
    NSString* templateName = [NSString stringWithFormat:@"CSharp/%@%@%@",
                              isDll ? @"Dll" : @"",
                              isMethod == YES ? @"Method":@"Property",
                              templateType];
    
    GRMustacheTemplate* template = [self LoadTemplate:templateName];
    
    return template;
}

@end
