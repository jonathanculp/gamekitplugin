//
//  PropertyParser.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "PropertyTokenizer.h"
#import "Token.h"

@implementation PropertyTokenizer
-(Token *) Next
{
    switch (mode) {
        case 0:
            [self ConsumeJunk];
            // Fallthrough
        case 1:
            return [self PropertyAttribute];
        case 2:
            return [self PropertyType];
        case 3:
            return [self PropertyName];
        case 4:
            // Consume the end of this stuff
            while ([self isDoneParsing] == NO) {
                [self next_c];
            }
            return NULL;
        default:
            return NULL;
    }
}

-(void) ConsumeJunk
{
    while(c != '(' && !doneParsing)
        [self next_c];
    [self next_c];
    mode = 1;
}

-(Token *) PropertyAttribute
{
    NSMutableString* attribute = [[NSMutableString alloc] init];

    while(c != ',' && c != ')' && !doneParsing){
        if([self isWhiteSpace]){
            [self next_c];
        }
        else {
            [attribute appendFormat:@"%c", c];
            [self next_c];
        }
    }
    
    if(c == ',')
        [self next_c];
    
    if(c == ')')
        mode = 2;
    
    Token* token = [[Token alloc] initWithTokenType:@"PropertyAttribute" withValue:attribute];
    return token;
}

-(Token *) PropertyType
{
    Token* token = [self ParseType];
    
    // If it's a type - next mode
    // But if it comes back as a type attribute, don't advance the mode
    // the type is coming up next
    
    if([token.Type isEqualToString:@"Type"])
    {
        mode = 3;
    }
    
    return token;
}

-(Token *) PropertyName
{
    NSMutableString* name = [[NSMutableString alloc] init];
    
    while([self isWhiteSpace] && !doneParsing)
        [self next_c];
    
    while([self isId] && !doneParsing)
    {
        [name appendFormat:@"%c", c];
        [self next_c];
    }
    
    mode = 4;
    
    Token* token = [[Token alloc] initWithTokenType:@"PropertyName" withValue:name];
    
    return token;
}
@end
