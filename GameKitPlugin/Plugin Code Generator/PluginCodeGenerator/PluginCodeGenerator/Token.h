//
//  Token.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Token : NSObject

@property (nonatomic, retain) NSString* Type;
@property (nonatomic, retain) NSString* Value;

-(id) initWithTokenType: (NSString*) type withValue:(NSString*) value;
@end

NS_ASSUME_NONNULL_END
