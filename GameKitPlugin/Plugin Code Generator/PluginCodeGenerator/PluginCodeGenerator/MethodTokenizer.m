//
//  MethodParser.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/1/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "MethodTokenizer.h"
#import "Token.h"

@implementation MethodTokenizer
-(Token*) Next
{
    Token* val = NULL;
    
    switch(mode)
    {
        case 0:
            val = [self ParseMethodType];
            break;
        case 1:
            val = [self ParseReturnType];
            break;
        case 2:
            val = [self ParseMethodName];
            break;
        case 3:
            val = [self ParseParamType];
            break;
        case 4:
            val = [self ParseParamName];
            break;
        case 5:
            val = [self ParseParamLabel];
            break;
    }
    
    if(val == NULL)
        doneParsing = YES;
    return val;
}

-(Token*) ParseMethodType
{
    [self next_c];
    
    NSString* methodType = NULL;
    if(c == '-')
        methodType = @"Instance";
    else if(c == '+')
        methodType = @"Class";
    else
        methodType = @"Unknown";
    
    [self next_c];
    
    mode++;
    return [[Token alloc] initWithTokenType:@"MethodType" withValue:methodType];
}

-(Token*) ParseMethodName
{
    NSString* val = [self ParseIdentifier];
    mode++;
    return [[Token alloc] initWithTokenType:@"MethodName" withValue:val];
}

-(Token*) ParseParamLabel
{
    NSString* val = [self ParseIdentifier];
    
    if(val == NULL)
        return NULL;
    
    mode -= 2;
    return [[Token alloc] initWithTokenType:@"Label" withValue:val];
}

-(Token*) ParseParamType
{
    Token* token = [self ParseType];
    
    // If it's a type attribute - stay in the same mode
    if([[token Type] isEqualToString:@"Type"])
        mode++;
    
    return token;
    //return [self ParseReturnType];
}

-(Token*) ParseParamName
{
    NSString* val = [self ParseIdentifier];
    
    // is this the end?
    if(c == ';')
        doneParsing = YES;
    
    mode++;
    return [[Token alloc] initWithTokenType:@"ParamName" withValue:val];
}

-(Token*) ParseReturnType
{
    while(c != '(')
    {
        if(doneParsing)
            return NULL;
        
        [self next_c];
    }
    
    [self next_c];
    
    NSMutableString* returnType = [NSMutableString string];
    
    while(c != ')' && !doneParsing)
    {
        if(![self isWhiteSpace])
            [returnType appendFormat:@"%c", c];
        [self next_c];
    }
    
    mode++;
    
    return [[Token alloc] initWithTokenType:@"ParamType" withValue:returnType];
}

-(NSString*) ParseIdentifier
{
    while(![self isId] && !doneParsing)
        [self next_c];
    
    if(!doneParsing){
        NSMutableString* returnType = [NSMutableString string];
        
        while([self isId] && !doneParsing){
            [returnType appendFormat:@"%c", c];
            [self next_c];
        }
        
        return returnType;
    }
    
    return NULL;
}
@end
