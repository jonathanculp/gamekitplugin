//
//  main.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 12/31/19.
//  Copyright © 2019 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <objc/runtime.h>
#import <CloudKit/CloudKit.h>
#import "Generator.h"
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdocumentation"
#import "GRMustache.h" //Hide a warning in this header because we don't want to change our dependencies
#pragma clang diagnostic pop
#import "ClassGenerator.h"
#import "CallbackGenerator.h"
#import "JSONLoader.h"

GRMustacheTemplate* classTemplate;
GRMustacheTemplate* headerTemplate;
GRMustacheTemplate* testTemplate;
GRMustacheTemplate* CSharpTemplate;

NSString* objcOutputDir;
NSString* csharpOutputDir;
NSString* csharpTestDir;


void generateDelegateObjects()
{
    // Load a single json file with all the delegate objects in it
    NSDictionary* classData = [JSONLoader LoadJSONData:@"./json/DelegateObjects/DelegateObjects.json"];
    
    if(classData == nil)
        return;
    
    // Make a mutable copy
    NSMutableDictionary* classDataCopy = [classData mutableCopy];
    
    ClassGenerator *generator = [[ClassGenerator alloc] init];
    
    NSMutableArray* newClassDataArray = [[NSMutableArray alloc] init];
    for(id class in classData[@"Classes"])
    {
        // Parse the class file
        NSDictionary* data = [generator MakeData:class];
        [newClassDataArray addObject:data];
        // replace the class entry with the parsed data
        
    }
    
    // overwrite the old data
    classDataCopy[@"Classes"] = newClassDataArray;
    
    // register every function in this delegate object with the plugin initializer
    // so C# can assign these function pointers
    
    GRMustacheTemplate* template = nil;
    
    template = LoadTemplate(@"ObjC/InitializerH");
    NSCAssert(template != nil, @"Couldn't load template");
    renderTemplateToFile(classDataCopy, template, @"GameKitInitializer.h", objcOutputDir);
    
    template = LoadTemplate(@"ObjC/InitializerMM");
    NSCAssert(template != nil, @"Couldn't load template");
    renderTemplateToFile(classDataCopy, template, @"GameKitInitializer.mm", objcOutputDir);
    
    template = LoadTemplate(@"CSharp/InitializerCS");
    NSCAssert(template != nil, @"Couldn't load template");
    renderTemplateToFile(classDataCopy, template, @"GameKitInitializer.cs", csharpOutputDir);
    
    // Okay those are the 3 required initializer classes
    
    // now make a C# class for each delegate object
    template = LoadTemplate(@"CSharp/DelegateObjectCS");
    NSCAssert(template != nil, @"Couldn't load template");
    
    for(id class in classDataCopy[@"Classes"])
    {
        NSString* className = class[@"ClassName"];
        renderTemplateToFile(class, template, [NSString stringWithFormat:@"%@.cs", className], csharpOutputDir);
    }
    
    // and an Objective-C class for each delegate object
    template = LoadTemplate(@"ObjC/DelegateObjectH");
    NSCAssert(template != nil, @"Couldnt' load template");
    
    for(id class in classDataCopy[@"Classes"])
    {
        NSString* className = class[@"ClassName"];
        renderTemplateToFile(class, template, [NSString stringWithFormat:@"%@.h", className], objcOutputDir);
    }
    
    template = LoadTemplate(@"ObjC/DelegateObjectMM");
    NSCAssert(template != nil, @"Couldnt' load template");
    
    for(id class in classDataCopy[@"Classes"])
    {
        NSString* className = class[@"ClassName"];
        renderTemplateToFile(class, template, [NSString stringWithFormat:@"%@.mm", className], objcOutputDir);
    }
}

int main(int argc, const char * argv[])
{
    NSFileManager *filemgr;
    NSString *currentpath;

    filemgr = [[NSFileManager alloc] init];
    currentpath = [filemgr currentDirectoryPath];

    NSLog(@"Current working directory is %@", currentpath);

    objcOutputDir = @"../../../LibraryProject/Common";
    csharpOutputDir = @"../../../UnityGameKitPlugin/Packages/GameKit/Runtime/";
    //csharpOutputDir = @"/Users/jonathan/Development/PongPrototype/PongUnity/Packages/GameKit/Runtime/Plugin/";
    csharpTestDir = @"../../../UnityGameKitPlugin/Packages/GameKit/Tests/Runtime/";

    NSCAssert([filemgr fileExistsAtPath:objcOutputDir], @"obj c output dir does not exist");
    NSCAssert([filemgr fileExistsAtPath:csharpOutputDir], @"c sharp output dir does not exist");
    NSCAssert([filemgr fileExistsAtPath:csharpTestDir], @"c sharp test dir does not exist");

    @autoreleasepool {

        classTemplate = LoadTemplate(@"ObjC/ClassImplementation");
        headerTemplate = LoadTemplate(@"ObjC/ClassHeader");
        CSharpTemplate = LoadTemplate(@"CSharp/CSharpClass");
        testTemplate = LoadTemplate(@"CSharp/CSharpTest");

        NSCAssert(classTemplate != nil, @"failed to find class template");
        NSCAssert(headerTemplate != nil, @"failed to find class header template");
        NSCAssert(CSharpTemplate != nil, @"failed to find csharp template");
        NSCAssert(testTemplate != nil, @"failed to find test template");

        NSError* error = nil;
        NSFileManager* fileMan = [NSFileManager defaultManager];
        [fileMan createDirectoryAtPath:@"./Build" withIntermediateDirectories:YES attributes:nil error:&error];
        NSCAssert(error == nil, @"failed to create build directory");

        NSArray* paths = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:@"./json/Classes" error:nil];
        NSArray* jsonFiles = [paths filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self ENDSWITH '.json'"]];

        __block BOOL didFailToGenerateAClass = NO;
        [jsonFiles enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSString* filename = obj;

            @try {
                generateClass(filename);
            }
            @catch(NSException* exc) {
                NSLog(@"failed to generate files for class %@", filename);
                NSLog(@"%@", [exc description]);
                didFailToGenerateAClass = YES;
            }
        }];

        //NSCAssert(didFailToGenerateAClass == NO, @"failed to generate one or more classes");

        NSArray* enums = [JSONLoader LoadJSONData:@"./json/Data/EnumTypes.json"];

        for(NSString* enumname in enums){
            @try {
                generateEnum(enumname);
            }
            @catch(NSException* exc)
            {
                NSLog(@"failed to generate files for enum %@", enumname);
                NSLog(@"%@", [exc description]);
            }
        }

        generateCallbacks(@"callbacks");
    }
    
    // Create all files needed to handle delegate objects like GKLocalPlayerListener
    // and GKMatchDelegate. This involves sending a bunch of function pointers from C#
    // and registering them in Objective-C
    
    generateDelegateObjects();
    
    return 0;
}

void renderTemplateToFile(id data, GRMustacheTemplate* template, NSString* filename, NSString* outputDir)
{
    NSError* anyError;
    
    NSString* rendering =
        [template renderObject:data error:&anyError];
    
    if(rendering == nil)
    {
        NSLog(@"%@", anyError);
    }

    NSString* classPath = [NSString stringWithFormat:@"%@/%@", outputDir, filename];
    BOOL result = [rendering writeToFile:classPath atomically:YES encoding:NSUTF8StringEncoding error:&anyError];
    
    if(result == NO)
    {
        NSLog(@"Failed to write file...%@", [anyError localizedDescription]);
    }
}

GRMustacheTemplate* LoadTemplate(NSString* name)
{
    NSError* anyError;
    
    NSString* filename = [NSString stringWithFormat:@"./Templates/%@.mustache", name];
    
    GRMustacheTemplate* template = [GRMustacheTemplate templateFromContentsOfFile:filename error:&anyError];
    
    if(template == NULL)
    {
        NSLog(@"%@", [anyError localizedDescription]);
    }
    
    return template;
}

void generateClass(NSString* filename)
{
    ClassGenerator *generator = [[ClassGenerator alloc] init];
    
    NSString* filepath = [NSString stringWithFormat:@"./json/Classes/%@", filename];
    NSDictionary* classData = [JSONLoader LoadJSONData:filepath];
    
    if([classData[@"Ignore"] isEqualTo:@"YES"]){
        NSLog(@"Skipping file...%@", filename);
        return;
    }
    
    NSLog(@"Generating %@...", filename);
    
    NSString* className = classData[@"ClassName"];
    
    if([filename containsString:className] == NO){
        NSException* invalidDataException = [NSException exceptionWithName:@"InvalidDataException" reason:@"filename does not match class name" userInfo:nil];
        @throw invalidDataException;
    }
    
    NSDictionary* data = [generator MakeData:classData];

    renderTemplateToFile(data, classTemplate, [NSString stringWithFormat:@"%@.mm", className], objcOutputDir);

    renderTemplateToFile(data, headerTemplate, [NSString stringWithFormat:@"%@.h", className], objcOutputDir);
    
    //renderTemplateToFile(data, testTemplate, [NSString stringWithFormat:@"Test%@.cs", className], csharpTestDir);
    
    renderTemplateToFile(data, CSharpTemplate, [NSString stringWithFormat:@"%@.cs", className], csharpOutputDir);
}

void generateEnum(NSString* enumName)
{
    GRMustacheTemplate* enumTemplate = LoadTemplate(@"CSharp/CSharpEnum");
    
    NSString* filepath = [NSString stringWithFormat:@"./json/Enums/%@.json", enumName];
    
    NSMutableDictionary* data = [[JSONLoader LoadJSONData:filepath] mutableCopy];
    Converters* con = [[Converters alloc] init];
    [con AddConverters:data];
    
    renderTemplateToFile(data, enumTemplate, [NSString stringWithFormat:@"%@.cs", enumName], csharpOutputDir);
}

void generateCallbacks(NSString* filename)
{
    Converters* con = [[Converters alloc] init];
    CallbackGenerator* gen = [[CallbackGenerator alloc] init];
    NSMutableDictionary* data = [[gen GenerateCallbacks] mutableCopy];
    [con AddConverters:data];
    
    GRMustacheTemplate* CSharpCallbackTemplate = LoadTemplate(@"CSharp/CSharpCallbacks");
    
    NSString* filepath = @"./json/Data/BlockData.json";
    NSMutableDictionary* blockdata = [[JSONLoader LoadJSONData:filepath] mutableCopy];
    [con AddConverters:blockdata];
    renderTemplateToFile(blockdata, CSharpCallbackTemplate, @"GameKitCallbacks.cs", csharpOutputDir);

    GRMustacheTemplate* ObjCCallbackTemplate = LoadTemplate(@"ObjC/ObjCCallbacks");
    renderTemplateToFile(data, ObjCCallbackTemplate, @"Callbacks.h", objcOutputDir);
}
