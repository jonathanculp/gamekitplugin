//
//  Utils.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 2/17/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Utils : NSObject
+(BOOL) IsDictionary:(NSString*) Type;
+(BOOL) IsPointer:(NSString*) Type;
+(BOOL) IsSet:(NSString*) Type;
+(BOOL) IsArray:(NSString*) Type;
+(BOOL) IsUntypedArray:(NSString*) Type;
+(BOOL) IsArrayOfPointers:(NSString*) Type;
+(BOOL) IsStringArray:(NSString*) Type;
+(BOOL) IsBlock:(NSString*) Type;
+(BOOL) IsMethodBlock:(NSString*) Type;
+(NSString*) ArraySubType:(NSString*) arrayDef;
+(NSString*) SetSubType:(NSString*) setDef;
+(NSString*) DictValueType:(NSString*) dictDef;
+(BOOL) IsInstanceType:(NSString*) Type;
+(NSString*) DictionaryKeyType:(NSString*) Type;
+(NSString*) DictionaryValueType:(NSString*) Type;
@end

NS_ASSUME_NONNULL_END
