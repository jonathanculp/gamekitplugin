//
//  MethodParser.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/2/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "MethodParser.h"
#import "MethodTokenizer.h"

@implementation MethodParser

-(NSDictionary*) ParseMethod: (NSString*) methodDecl
{
    NSMutableArray* params = [[NSMutableArray alloc] init];
    MethodTokenizer* parser = [[MethodTokenizer alloc] initWithString:methodDecl];
    
    NSString* methodType = [[parser Next] Value];
    NSString* retType = [[parser Next] Value];
    NSString* methodName = [[parser Next] Value];
    
    Token* token = [parser Next];
    int index = 0;
    
    while(token != NULL)
    {
        NSMutableDictionary* paramInfo = [[NSMutableDictionary alloc]init];
        
        if([[token Type] isEqual:@"Label"])
        {
            NSString* label = [token Value];
            [paramInfo setObject:label forKey:@"ParamLabel"];
            
            token = [parser Next];
        }
        
        while ([[token Type] isEqualToString:@"TypeAttribute"])
        {
            // This may have to be an attrbitue array eventually
            [paramInfo setObject:[token Value] forKey:@"Attr"];
            token = [parser Next];
        }
        
        NSString* type = [token Value];
        
        token = [parser Next];
        NSString* name = [token Value];
        
        [paramInfo setObject:type forKey:@"Type"];
        [paramInfo setObject:name forKey:@"Name"];
        [paramInfo setObject:[NSNumber numberWithInt:index] forKey:@"Index"];
        index++;
        
        token = [parser Next];
        
        [params addObject:paramInfo];
    }
    
    return @{
        @"Decl":methodDecl,
        @"MethodType":methodType,
        @"MethodName":methodName,
        @"ReturnParam":@{@"Type":retType,@"Name":@"val"},
        @"Parameters":params
    };
}
@end
