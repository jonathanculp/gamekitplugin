//
//  CallbackGenerator.h
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/20/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Converters.h"

NS_ASSUME_NONNULL_BEGIN

@interface CallbackGenerator : NSObject
{
    Converters* converter;
}

-(NSDictionary*) GenerateCallbacks;

@end

NS_ASSUME_NONNULL_END
