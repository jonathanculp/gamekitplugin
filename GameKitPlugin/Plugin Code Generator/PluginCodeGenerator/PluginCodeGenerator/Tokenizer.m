//
//  Parser.m
//  PluginCodeGenerator
//
//  Created by Jonathan on 1/8/20.
//  Copyright © 2020 HovelHouseApps. All rights reserved.
//

#import "Tokenizer.h"
#import "Token.h"

@implementation Tokenizer
-(id) initWithString: (NSString*) decl
{
    if(self = [super init])
    {
        len = [decl length];
        buffer = (unichar*)calloc(len + 1, sizeof(unichar));
        [decl getCharacters:buffer range:NSMakeRange(0, len)];
        
        identifierChars = [NSCharacterSet characterSetWithCharactersInString:@"_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789<>"];

        typeChars = [NSCharacterSet characterSetWithCharactersInString:@"_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789<>*"];
    }
    
    return self;
}

-(void) next_c
{
    if(itr < len)
    {
        c = buffer[itr];
        itr++;
    }
    else
    {
        doneParsing = YES;
    }
}

-(BOOL) isId
{
    return [identifierChars characterIsMember:c];
}

-(BOOL) isType
{
    return [typeChars characterIsMember:c];
}

-(BOOL) isWhiteSpace
{
    return [[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:c];
}

-(BOOL) isDoneParsing
{
    return doneParsing;
}

-(Token*) ParseType
{
    NSInteger arrowCount = 0;
    NSMutableString* type = [[NSMutableString alloc] init];
    while([self isId] == false && !doneParsing)
        [self next_c];
    
    while(!doneParsing)
    {
        if(c == '<')
        {
            arrowCount++;
        }
        else if(c == '>')
        {
            arrowCount--;
        }
            
        if(arrowCount > 0){
            if([self isWhiteSpace] == NO)
                [type appendFormat:@"%c", c];
            [self next_c];
        }
        else {
            //if([self isWhiteSpace] == NO){
            if([self isId]){
                [type appendFormat:@"%c", c];
                [self next_c];
            }
            else
                break;
        }
    }
    
    // See if it's a type attribute
    if([type isEqualToString:@"nonnull"] || [type isEqualToString:@"nullable"])
    {
        Token* token = [[Token alloc] initWithTokenType:@"TypeAttribute" withValue:type];
        return token;
    }
    
    while([self isWhiteSpace] && !doneParsing)
        [self next_c];
    
    while(c == '*' && !doneParsing){
        [type appendFormat:@"%c", c];
        [self next_c];
    }
    
    if(type == nil || [type isEqualTo:@""])
        return nil;
    
    return [[Token alloc] initWithTokenType:@"Type" withValue:type];
}
@end
