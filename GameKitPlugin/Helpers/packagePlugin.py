#!/usr/bin/python

import os, sys, getopt, distutils, shutil
from distutils import dir_util

def main(argv):
	inputdir = ''
	outputdir = ''
	distrodir = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:d:")
	except getopt.GetoptError:
		print 'pagkagePlugin.y -i <inputdir> -o <outputdir> -d <distrodir>'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print 'packagePlugin.py - <inputdir> -o <outputdir> -d <distrodir>'
			sys.exit()
		elif opt == '-i':
			print arg
			inputdir = arg
		elif opt == '-o':
			outputdir = arg
		elif opt == '-d':
			distrodir = arg

	print 'Packaging the plugin...', inputdir, outputdir

	inpputpackagedir = os.path.join(inputdir, './Packages/GameKit')
	outputpackagedir = os.path.join(outputdir, './Packages/GameKit')
	print 'Copying ', inpputpackagedir, ' to ', outputpackagedir
	distutils.dir_util.copy_tree(inpputpackagedir, outputpackagedir)

	distropackagedir = os.path.join(distrodir, './Assets/Plugins/GameKit/GameKit')
	#todo - remove old parent directory

	print 'Copying package to distro project'
	distutils.dir_util.copy_tree(outputpackagedir, distropackagedir)

	print 'Copying readme to parent directory'
	shutil.copy(os.path.join(distropackagedir, './README.MD'), os.path.join(distropackagedir, '../README.MD'))

	print 'Copying samples to distro project'
	outputsamplesdir = os.path.join(outputdir, './Assets/Samples')
	distrosamplesdir = os.path.join(distrodir, './Assets/Plugins/GameKit/GameKit/Samples~')
	distutils.dir_util.copy_tree(outputsamplesdir, distrosamplesdir)
	
	print 'Zipping it all up'
	shutil.make_archive(os.path.join(distropackagedir,'../GameKit'), 'zip', distropackagedir)

	print 'Done archiving. Deleting directory'
	shutil.rmtree(distropackagedir)

if __name__ == "__main__":
	main(sys.argv[1:])